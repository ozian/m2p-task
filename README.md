# task

A flutter project to serve as a submission for the task assigned as part of the recruitment process at M2P Fintech.

## Getting Started

This project focuses on demonstrating implementation of various technologies, features, industry standards and best practices relevant for the scope of the assigned task.
It however does not focus build quality, optimisations, production readiness, security standards in order to allow a demonstration of the core skillset in an efficient timeframe. Hence, its presentation should only be consumed as an MVP and not a polished release.

## Implementation Scope

1. Splash Screen (branding image, background colour)
2. Login Screen with OTP Verification (state managed using RiverPod)
3. Form Data Display Screen
4. Form Data Create/Edit Screen (implements dynamic forms)
5. API Snapshot Mocked
7. JSON compliant models generated using json_serializable
7. MVVM Architecture followed
7. Simple routing setup using GoRouter
8. Dependency injection setup using GetIt
10. Dynamic forms generated using flutter_form_builder
11. valueExpression evaluated using dart_eval

## Tested Platforms

Android, iOS, macOS

## Testing Credentials

Stubs are made to allow testing of simple state logic through mocks.
Throughout the code, you shall find the comment `// stub` that denotes such instances.

Mobile Number: "9876543210"
OTP: "1234"