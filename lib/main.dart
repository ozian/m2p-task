import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:task/core/constants.dart';
import 'package:task/core/di.dart';
import 'package:task/core/router.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupDi();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerConfig: router,
      title: 'M2P Task',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: scaffoldBgColor),
        scaffoldBackgroundColor: scaffoldBgColor,
        canvasColor: bgColor,
        hintColor: hintColor,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all(buttonBgColor),
            foregroundColor: WidgetStateProperty.all(Colors.white),
          ),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all(Colors.transparent),
            foregroundColor: WidgetStateProperty.all(buttonBgColor),
            side: WidgetStateProperty.all(
              const BorderSide(
                color: buttonBgColor,
                width: 2,
              ),
            ),
          ),
        ),
        filledButtonTheme: FilledButtonThemeData(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all(buttonBgColor),
            foregroundColor: WidgetStateProperty.all(Colors.white),
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          // hintStyle: const TextStyle(color: hintColor, fontSize: 14.0),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(defaultPadding / 2),
            borderSide: const BorderSide(
              color: borderColor,
              width: 3,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(defaultPadding / 2),
            borderSide: const BorderSide(
              color: borderColor,
              width: 1.4,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(defaultPadding / 2),
            borderSide: const BorderSide(
              color: borderColor,
              width: 1.4,
            ),
          ),
        ),
      ),
    );
  }
}
