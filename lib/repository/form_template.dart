import 'package:dartz/dartz.dart';
import 'package:task/data/failure.dart';
import 'package:task/data/mock_api.dart';
import 'package:task/model/form_template.dart';

class FormTemplateRepository {
  FormTemplateRepository();
  Future<Either<Failure, FormTemplateModel>> getFormTemplate(
      String accessToken, String formKey, int clientId) async {
    try {
      final response = await MockApi.getFormTemplate(accessToken, formKey);
      final formTemplate = FormTemplateModel.fromJson(response);
      return Right(formTemplate);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }
}
