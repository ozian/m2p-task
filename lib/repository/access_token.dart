import 'package:dartz/dartz.dart';
import 'package:task/data/failure.dart';
import 'package:task/data/mock_api.dart';
import 'package:task/model/access_token.dart';

class AccessTokenRepository {
  AccessTokenRepository();

  Future<Either<Failure, AccessTokenResponseModel>> getAccessToken(
      String mobileNumber, String otp) async {
    try {
      final response = await MockApi.fetchAccessToken(mobileNumber, otp);
      final accessToken = AccessTokenResponseModel.fromJson(response);
      return Right(accessToken);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }
}
