import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task/core/di.dart';
import 'package:task/data/failure.dart';
import 'package:task/data/mock_api.dart';
import 'package:task/model/login.dart';

class LoginRepository {
  LoginRepository();

  Future<Either<Failure, LoginResponseModel>> login(String mobileNumber) async {
    try {
      final response = await MockApi.login(mobileNumber);
      final user = LoginResponseModel.fromJson(response);
      return Right(user);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }

  Future<Either<Failure, bool>> logout() async {
    try {
      final accessToken =
          await injector<SharedPreferences>().remove('accessToken');
      final clientData =
          await injector<SharedPreferences>().remove('clientData');
      return Right(accessToken && clientData);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }
}
