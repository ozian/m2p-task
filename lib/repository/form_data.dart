import 'package:dartz/dartz.dart';
import 'package:task/data/failure.dart';
import 'package:task/data/mock_api.dart';
import 'package:task/model/form_data_item.dart';

class FormDataRepository {
  FormDataRepository();

  Future<Either<Failure, FormDataResponseModel>> getFormData(
      String accessToken, String formKey, int clientId) async {
    try {
      final response =
          await MockApi.getFormData(accessToken, formKey, clientId);
      final formData = FormDataResponseModel.fromJson(response);
      return Right(formData);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }

  Future<Either<Failure, FormDataMutationResponseModel>> updateFormData(
      String accessToken,
      String formKey,
      int clientId,
      FormDataRequestModel request) async {
    try {
      final response = await MockApi.updateFormData(
          accessToken, formKey, clientId, request.toJson());
      final formData = FormDataMutationResponseModel.fromJson(response);
      return Right(formData);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }

  Future<Either<Failure, FormDataMutationResponseModel>> postFormData(
      String accessToken,
      String formKey,
      int clientId,
      FormDataRequestModel request) async {
    try {
      final response = await MockApi.postFormData(
          accessToken, formKey, clientId, request.toJson());
      final formData = FormDataMutationResponseModel.fromJson(response);
      return Right(formData);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }
}
