import 'package:dartz/dartz.dart';
import 'package:task/data/failure.dart';
import 'package:task/data/mock_api.dart';
import 'package:task/model/client_data.dart';

class ClientDataRepository {
  ClientDataRepository();

  Future<Either<Failure, ClientDataModel>> getClientData(
      String accessToken) async {
    try {
      final response = await MockApi.getClientData(accessToken);
      final clientData = ClientDataModel.fromJson(response);
      return Right(clientData);
    } catch (e) {
      return Left(Failure(e.toString()));
    }
  }
}
