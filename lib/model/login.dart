import 'package:json_annotation/json_annotation.dart';
part 'login.g.dart';

@JsonSerializable()
class LoginResponseModel {
  final Map changes;
  final String resourceIdentifier;

  const LoginResponseModel(
      {required this.changes, required this.resourceIdentifier});
  factory LoginResponseModel.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$LoginResponseModelToJson(this);
}
