// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponseModel _$LoginResponseModelFromJson(Map<String, dynamic> json) =>
    LoginResponseModel(
      changes: json['changes'] as Map<String, dynamic>,
      resourceIdentifier: json['resourceIdentifier'] as String,
    );

Map<String, dynamic> _$LoginResponseModelToJson(LoginResponseModel instance) =>
    <String, dynamic>{
      'changes': instance.changes,
      'resourceIdentifier': instance.resourceIdentifier,
    };
