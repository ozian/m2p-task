// ignore_for_file: non_constant_identifier_names

import 'package:json_annotation/json_annotation.dart';
part 'access_token.g.dart';

@JsonSerializable()
class AccessTokenResponseModel {
  final String access_token;
  final String token_type;
  final String refresh_token;
  final int expires_in;
  final String scope;
  const AccessTokenResponseModel({
    required this.access_token,
    required this.token_type,
    required this.refresh_token,
    required this.expires_in,
    required this.scope,
  });

  factory AccessTokenResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AccessTokenResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$AccessTokenResponseModelToJson(this);
}
