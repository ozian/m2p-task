// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClientDataModel _$ClientDataModelFromJson(Map<String, dynamic> json) =>
    ClientDataModel(
      username: json['username'] as String,
      userId: (json['userId'] as num).toInt(),
      accessToken: json['accessToken'] as String,
      authenticated: json['authenticated'] as bool,
      officeId: (json['officeId'] as num).toInt(),
      officeName: json['officeName'] as String,
      roles: (json['roles'] as List<dynamic>)
          .map((e) => RoleModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      permissions: (json['permissions'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      shouldRenewPassword: json['shouldRenewPassword'] as bool,
      clientId: (json['clientId'] as num).toInt(),
    );

Map<String, dynamic> _$ClientDataModelToJson(ClientDataModel instance) =>
    <String, dynamic>{
      'username': instance.username,
      'userId': instance.userId,
      'accessToken': instance.accessToken,
      'authenticated': instance.authenticated,
      'officeId': instance.officeId,
      'officeName': instance.officeName,
      'roles': instance.roles.map((e) => e.toJson()).toList(),
      'permissions': instance.permissions,
      'shouldRenewPassword': instance.shouldRenewPassword,
      'clientId': instance.clientId,
    };

RoleModel _$RoleModelFromJson(Map<String, dynamic> json) => RoleModel(
      id: (json['id'] as num).toInt(),
      name: json['name'] as String,
      description: json['description'] as String,
      disabled: json['disabled'] as bool,
      roleBasedLimits: json['roleBasedLimits'] as List<dynamic>,
    );

Map<String, dynamic> _$RoleModelToJson(RoleModel instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'disabled': instance.disabled,
      'roleBasedLimits': instance.roleBasedLimits,
    };
