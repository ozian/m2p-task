// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'access_token.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccessTokenResponseModel _$AccessTokenResponseModelFromJson(
        Map<String, dynamic> json) =>
    AccessTokenResponseModel(
      access_token: json['access_token'] as String,
      token_type: json['token_type'] as String,
      refresh_token: json['refresh_token'] as String,
      expires_in: (json['expires_in'] as num).toInt(),
      scope: json['scope'] as String,
    );

Map<String, dynamic> _$AccessTokenResponseModelToJson(
        AccessTokenResponseModel instance) =>
    <String, dynamic>{
      'access_token': instance.access_token,
      'token_type': instance.token_type,
      'refresh_token': instance.refresh_token,
      'expires_in': instance.expires_in,
      'scope': instance.scope,
    };
