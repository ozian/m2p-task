// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_data_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FormDataItemModel _$FormDataItemModelFromJson(Map<String, dynamic> json) =>
    FormDataItemModel(
      id: (json['id'] as num).toInt(),
      name: json['name'] as String,
      value: json['value'] as String,
    );

Map<String, dynamic> _$FormDataItemModelToJson(FormDataItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'value': instance.value,
    };

FormDataResponseModel _$FormDataResponseModelFromJson(
        Map<String, dynamic> json) =>
    FormDataResponseModel(
      payload: (json['payload'] as List<dynamic>)
          .map((e) => FormDataItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      resourceId: (json['resourceId'] as num).toInt(),
    );

Map<String, dynamic> _$FormDataResponseModelToJson(
        FormDataResponseModel instance) =>
    <String, dynamic>{
      'payload': instance.payload.map((e) => e.toJson()).toList(),
      'resourceId': instance.resourceId,
    };

FormDataRequestModel _$FormDataRequestModelFromJson(
        Map<String, dynamic> json) =>
    FormDataRequestModel(
      payload: (json['payload'] as List<dynamic>)
          .map((e) => FormDataItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      leadId: (json['leadId'] as num).toInt(),
    );

Map<String, dynamic> _$FormDataRequestModelToJson(
        FormDataRequestModel instance) =>
    <String, dynamic>{
      'payload': instance.payload.map((e) => e.toJson()).toList(),
      'leadId': instance.leadId,
    };

FormDataMutationResponseModel _$FormDataMutationResponseModelFromJson(
        Map<String, dynamic> json) =>
    FormDataMutationResponseModel(
      resourceId: (json['resourceId'] as num).toInt(),
      rollbackTransaction: json['rollbackTransaction'] as bool,
    );

Map<String, dynamic> _$FormDataMutationResponseModelToJson(
        FormDataMutationResponseModel instance) =>
    <String, dynamic>{
      'resourceId': instance.resourceId,
      'rollbackTransaction': instance.rollbackTransaction,
    };
