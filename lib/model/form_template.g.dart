// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_template.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FormTemplateModel _$FormTemplateModelFromJson(Map<String, dynamic> json) =>
    FormTemplateModel(
      formKey: json['formKey'] as String,
      formName: json['formName'] as String,
      description: json['description'] as String,
      entityType: json['entityType'] as String,
      isBaseTable: json['isBaseTable'] as bool,
      multipleAllowed: json['multipleAllowed'] as bool,
      parameters: (json['parameters'] as List<dynamic>)
          .map((e) =>
              FormTemplateParameterModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      categories: (json['categories'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$FormTemplateModelToJson(FormTemplateModel instance) =>
    <String, dynamic>{
      'formKey': instance.formKey,
      'formName': instance.formName,
      'description': instance.description,
      'entityType': instance.entityType,
      'isBaseTable': instance.isBaseTable,
      'multipleAllowed': instance.multipleAllowed,
      'parameters': instance.parameters.map((e) => e.toJson()).toList(),
      'categories': instance.categories,
    };

FormTemplateParameterModel _$FormTemplateParameterModelFromJson(
        Map<String, dynamic> json) =>
    FormTemplateParameterModel(
      id: (json['id'] as num).toInt(),
      name: json['name'] as String,
      displayName: json['displayName'] as String,
      dataType: json['dataType'] as String,
      possibleValues: json['possibleValues'] as List<dynamic>?,
      possibleValuesMap:
          PossibleValueModel.fromString(json['possibleValuesMap'] as String?),
      isMandatory: json['isMandatory'] as bool,
      isEditable: json['isEditable'] as bool,
      isHidden: json['isHidden'] as bool,
      isAdditionalField: json['isAdditionalField'] as bool,
      valueExpression: json['valueExpression'] as String?,
      displayOrder: (json['displayOrder'] as num).toInt(),
      length: (json['length'] as num).toInt(),
      categoryValue: json['categoryValue'] as String,
      category: json['category'] as String,
      defaultSelection: json['defaultSelection'] as String?,
    );

Map<String, dynamic> _$FormTemplateParameterModelToJson(
    FormTemplateParameterModel instance) {
  final val = <String, dynamic>{
    'id': instance.id,
    'name': instance.name,
    'displayName': instance.displayName,
    'dataType': instance.dataType,
    'possibleValues': instance.possibleValues,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('possibleValuesMap', instance.possibleValuesMap?.toJson());
  val['isMandatory'] = instance.isMandatory;
  val['isEditable'] = instance.isEditable;
  val['isHidden'] = instance.isHidden;
  val['isAdditionalField'] = instance.isAdditionalField;
  val['valueExpression'] = instance.valueExpression;
  val['displayOrder'] = instance.displayOrder;
  val['length'] = instance.length;
  val['categoryValue'] = instance.categoryValue;
  val['category'] = instance.category;
  val['defaultSelection'] = instance.defaultSelection;
  return val;
}

PossibleValueModel _$PossibleValueModelFromJson(Map<String, dynamic> json) =>
    PossibleValueModel(
      dependantFieldName: json['dependantFieldName'] as String,
      value: (json['value'] as Map<String, dynamic>).map(
        (k, e) =>
            MapEntry(k, (e as List<dynamic>).map((e) => e as String).toList()),
      ),
    );

Map<String, dynamic> _$PossibleValueModelToJson(PossibleValueModel instance) =>
    <String, dynamic>{
      'dependantFieldName': instance.dependantFieldName,
      'value': instance.value,
    };
