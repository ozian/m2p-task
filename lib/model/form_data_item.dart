import 'package:json_annotation/json_annotation.dart';
part 'form_data_item.g.dart';

@JsonSerializable(explicitToJson: true)
class FormDataItemModel {
  final int id;
  final String name;
  final String value;

  const FormDataItemModel(
      {required this.id, required this.name, required this.value});

  factory FormDataItemModel.fromJson(Map<String, dynamic> json) =>
      _$FormDataItemModelFromJson(json);
  Map<String, dynamic> toJson() => _$FormDataItemModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FormDataResponseModel {
  final List<FormDataItemModel> payload;
  final int resourceId;

  const FormDataResponseModel(
      {required this.payload, required this.resourceId});

  factory FormDataResponseModel.fromJson(Map<String, dynamic> json) =>
      _$FormDataResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$FormDataResponseModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FormDataRequestModel {
  final List<FormDataItemModel> payload;
  final int leadId;

  const FormDataRequestModel({required this.payload, required this.leadId});

  factory FormDataRequestModel.fromJson(Map<String, dynamic> json) =>
      _$FormDataRequestModelFromJson(json);
  Map<String, dynamic> toJson() => _$FormDataRequestModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FormDataMutationResponseModel {
  final int resourceId;
  final bool rollbackTransaction;

  const FormDataMutationResponseModel(
      {required this.resourceId, required this.rollbackTransaction});

  factory FormDataMutationResponseModel.fromJson(Map<String, dynamic> json) =>
      _$FormDataMutationResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$FormDataMutationResponseModelToJson(this);
}
