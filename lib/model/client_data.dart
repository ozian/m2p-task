import 'package:json_annotation/json_annotation.dart';
part 'client_data.g.dart';

@JsonSerializable(explicitToJson: true)
class ClientDataModel {
  final String username;
  final int userId;
  final String accessToken;
  final bool authenticated;
  final int officeId;
  final String officeName;
  final List<RoleModel> roles;
  final List<String> permissions;
  final bool shouldRenewPassword;
  final int clientId;

  const ClientDataModel({
    required this.username,
    required this.userId,
    required this.accessToken,
    required this.authenticated,
    required this.officeId,
    required this.officeName,
    required this.roles,
    required this.permissions,
    required this.shouldRenewPassword,
    required this.clientId,
  });

  factory ClientDataModel.fromJson(Map<String, dynamic> json) =>
      _$ClientDataModelFromJson(json);
  Map<String, dynamic> toJson() => _$ClientDataModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RoleModel {
  final int id;
  final String name;
  final String description;
  final bool disabled;
  final List<dynamic> roleBasedLimits;

  const RoleModel({
    required this.id,
    required this.name,
    required this.description,
    required this.disabled,
    required this.roleBasedLimits,
  });

  factory RoleModel.fromJson(Map<String, dynamic> json) =>
      _$RoleModelFromJson(json);
  Map<String, dynamic> toJson() => _$RoleModelToJson(this);
}
