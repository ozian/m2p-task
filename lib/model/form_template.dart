import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
part 'form_template.g.dart';

@JsonSerializable(explicitToJson: true)
class FormTemplateModel {
  final String formKey;
  final String formName;
  final String description;
  final String entityType;
  final bool isBaseTable;
  final bool multipleAllowed;
  final List<FormTemplateParameterModel> parameters;
  final List<String> categories;

  const FormTemplateModel({
    required this.formKey,
    required this.formName,
    required this.description,
    required this.entityType,
    required this.isBaseTable,
    required this.multipleAllowed,
    required this.parameters,
    required this.categories,
  });

  factory FormTemplateModel.fromJson(Map<String, dynamic> json) =>
      _$FormTemplateModelFromJson(json);
  Map<String, dynamic> toJson() => _$FormTemplateModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FormTemplateParameterModel {
  final int id;
  final String name;
  final String displayName;
  final String dataType;
  final List<dynamic>? possibleValues;
  @JsonKey(fromJson: PossibleValueModel.fromString, includeIfNull: false)
  final PossibleValueModel? possibleValuesMap;
  final bool isMandatory;
  final bool isEditable;
  final bool isHidden;
  final bool isAdditionalField;
  final String? valueExpression;
  final int displayOrder;
  final int length;
  final String categoryValue;
  final String category;
  final String? defaultSelection;

  const FormTemplateParameterModel({
    required this.id,
    required this.name,
    required this.displayName,
    required this.dataType,
    this.possibleValues,
    this.possibleValuesMap,
    required this.isMandatory,
    required this.isEditable,
    required this.isHidden,
    required this.isAdditionalField,
    this.valueExpression,
    required this.displayOrder,
    required this.length,
    required this.categoryValue,
    required this.category,
    required this.defaultSelection,
  });

  factory FormTemplateParameterModel.fromJson(Map<String, dynamic> json) =>
      _$FormTemplateParameterModelFromJson(json);
  Map<String, dynamic> toJson() => _$FormTemplateParameterModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class PossibleValueModel {
  final String dependantFieldName;
  final Map<String, List<String>> value;
  const PossibleValueModel({
    required this.dependantFieldName,
    required this.value,
  });

  factory PossibleValueModel.fromJson(Map<String, dynamic> json) =>
      _$PossibleValueModelFromJson(json);
  Map<String, dynamic> toJson() => _$PossibleValueModelToJson(this);
  static PossibleValueModel? fromString(String? jsonString) =>
      jsonString == null
          ? null
          : _$PossibleValueModelFromJson(jsonDecode(jsonString));
}
