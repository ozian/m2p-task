import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:task/core/constants.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:task/core/di.dart';
import 'package:task/view_model/login.dart';

class LoginScreen extends ConsumerWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    TextEditingController mobileNumberController = TextEditingController(
        text: ref
            .read(injector<ChangeNotifierProvider<LoginViewModel>>())
            .mobileNumber);
    TextEditingController otpController = TextEditingController(
      text: ref.read(injector<ChangeNotifierProvider<LoginViewModel>>()).otp,
    );
    return Scaffold(
      appBar: ref
              .watch(injector<ChangeNotifierProvider<LoginViewModel>>())
              .isOtpScreen
          ? AppBar(
              title: Text(
                "Verification",
                style: Theme.of(context)
                    .textTheme
                    .labelLarge
                    ?.copyWith(color: Colors.white),
              ),
              centerTitle: false,
              backgroundColor: scaffoldBgColor,
              leading: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.white.withOpacity(0.25),
                  child: IconButton(
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      ref
                          .read(injector<
                              ChangeNotifierProvider<LoginViewModel>>())
                          .returnToLogin();
                    },
                  ),
                ),
              ),
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.only(
          top: defaultPadding * 10,
          left: defaultPadding * 2,
          right: defaultPadding * 2,
        ),
        child: RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: "Hi there 👋\n",
                style: Theme.of(context)
                    .textTheme
                    .titleMedium
                    ?.copyWith(color: Colors.white),
              ),
              TextSpan(
                text: "Login to start your work!",
                style: Theme.of(context)
                    .textTheme
                    .bodySmall
                    ?.copyWith(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
      // backgroundColor: bgColor,
      bottomSheet: BottomSheet(
        elevation: 5,
        onClosing: () {},
        builder: (context) {
          switch (ref
              .watch(injector<ChangeNotifierProvider<LoginViewModel>>())
              .isOtpScreen) {
            case true:
              return Padding(
                padding: const EdgeInsets.all(defaultPadding * 2),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    const Spacer(),
                    if (ref
                        .watch(
                            injector<ChangeNotifierProvider<LoginViewModel>>())
                        .isLoading)
                      const CircularProgressIndicator(),
                    if (!ref
                        .watch(
                            injector<ChangeNotifierProvider<LoginViewModel>>())
                        .isLoading) ...[
                      const Text("Enter the 4 digit OTP sent to"),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            ref
                                .watch(injector<
                                    ChangeNotifierProvider<LoginViewModel>>())
                                .mobileNumber,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                          TextButton(
                            onPressed: () {
                              ref
                                  .watch(injector<
                                      ChangeNotifierProvider<LoginViewModel>>())
                                  .returnToLogin();
                            },
                            child: Text(
                              "Edit",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: defaultPadding * 2),
                      IntrinsicWidth(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "OTP",
                              style: Theme.of(context).textTheme.bodySmall,
                            ),
                            TextFormField(
                              controller: otpController,
                              keyboardType: TextInputType
                                  .number, // Set the keyboard type to numeric
                              inputFormatters: <TextInputFormatter>[
                                FilteringTextInputFormatter
                                    .digitsOnly // Allow only digits
                              ],
                              enabled: !ref
                                  .watch(injector<
                                      ChangeNotifierProvider<LoginViewModel>>())
                                  .isLoading,
                              onFieldSubmitted: (value) {
                                ref
                                    .read(injector<
                                        ChangeNotifierProvider<
                                            LoginViewModel>>())
                                    .getAccessToken(value);
                              },
                              decoration: const InputDecoration(
                                hintText: "4 digit OTP",
                                counterText: "",
                              ),
                              maxLength: 4,
                            ),
                            const SizedBox(height: defaultPadding),
                            const Center(
                              child: OTPTimer(),
                            ),
                          ],
                        ),
                      ),
                    ],
                    Expanded(
                      child: Center(
                        child: Text(
                          ref
                              .watch(injector<
                                  ChangeNotifierProvider<LoginViewModel>>())
                              .error,
                          overflow: TextOverflow.fade,
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall
                              ?.copyWith(color: Colors.red),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: OutlinedButton(
                            onPressed: !ref
                                    .watch(injector<
                                        ChangeNotifierProvider<
                                            LoginViewModel>>())
                                    .isLoading
                                ? () {
                                    ref
                                        .read(injector<
                                            ChangeNotifierProvider<
                                                LoginViewModel>>())
                                        .login(ref
                                            .read(injector<
                                                ChangeNotifierProvider<
                                                    LoginViewModel>>())
                                            .mobileNumber);
                                  }
                                : null,
                            child: const Text("Resend OTP"),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: defaultPadding),
                    Row(
                      children: [
                        Expanded(
                          child: FilledButton(
                            onPressed: !ref
                                    .watch(injector<
                                        ChangeNotifierProvider<
                                            LoginViewModel>>())
                                    .isLoading
                                ? () {
                                    ref
                                        .read(injector<
                                            ChangeNotifierProvider<
                                                LoginViewModel>>())
                                        .getAccessToken(otpController.text);
                                  }
                                : null,
                            child: const Text("Verify Mobile Number"),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            case false:
              return Padding(
                padding: const EdgeInsets.all(defaultPadding * 2),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      controller: mobileNumberController,
                      enabled: !ref
                          .watch(injector<
                              ChangeNotifierProvider<LoginViewModel>>())
                          .isLoading,
                      onFieldSubmitted: (value) {
                        ref
                            .read(injector<
                                ChangeNotifierProvider<LoginViewModel>>())
                            .login(value);
                      },
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(color: Colors.black),
                      decoration: const InputDecoration(
                          hintText: "Registered Mobile Number"),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(defaultPadding),
                      child: Center(
                        child: Text(
                          ref
                              .watch(injector<
                                  ChangeNotifierProvider<LoginViewModel>>())
                              .error,
                          overflow: TextOverflow.fade,
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall
                              ?.copyWith(color: Colors.red),
                        ),
                      ),
                    ),
                    ref
                            .watch(injector<
                                ChangeNotifierProvider<LoginViewModel>>())
                            .isLoading
                        ? const CircularProgressIndicator()
                        : Row(
                            children: [
                              Expanded(
                                child: ElevatedButton(
                                  onPressed: () {
                                    ref
                                        .read(injector<
                                            ChangeNotifierProvider<
                                                LoginViewModel>>())
                                        .login(mobileNumberController.text);
                                  },
                                  child: const Text("Login"),
                                ),
                              ),
                            ],
                          ),
                  ],
                ),
              );
          }
        },
        enableDrag: false,
      ),
    );
  }
}

class OTPTimer extends StatefulWidget {
  const OTPTimer({super.key});

  @override
  State<OTPTimer> createState() => _OTPTimerState();
}

class _OTPTimerState extends State<OTPTimer> {
  late Timer timer;
  int secondsRemaining = 30;

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void startTimer() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (secondsRemaining > 0) {
          secondsRemaining--;
        } else {
          timer.cancel();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      "${(secondsRemaining ~/ 60).toString().padLeft(2, '0')}:${(secondsRemaining % 60).toString().padLeft(2, '0')}",
      style: Theme.of(context)
          .textTheme
          .bodyLarge
          ?.copyWith(color: scaffoldBgColor),
    );
  }
}
