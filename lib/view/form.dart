import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:task/core/constants.dart';
import 'package:task/core/di.dart';
import 'package:task/core/router.dart';
import 'package:task/model/form_data_item.dart';
import 'package:task/model/form_template.dart';
import 'package:task/repository/login.dart';
import 'package:task/view_model/forms.dart';

class FormScreen extends ConsumerStatefulWidget {
  const FormScreen({super.key});

  @override
  ConsumerState<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends ConsumerState<FormScreen> {
  @override
  void initState() {
    super.initState();
    ref
        .read(injector<ChangeNotifierProvider<FormsViewModel>>())
        .initialize(formKey: '404');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          'Dynamic Form',
          style: Theme.of(context)
              .textTheme
              .labelLarge
              ?.copyWith(color: Colors.white),
        ),
        backgroundColor: scaffoldBgColor,
        actions: [
          IconButton(
            onPressed: () async {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return const AlertDialog(
                    content: CircularProgressIndicator(),
                  );
                },
              );
              ref
                  .read(injector<ChangeNotifierProvider<FormsViewModel>>())
                  .clear();
              final response = await injector<LoginRepository>().logout();
              response.fold(
                (e) {
                  if (kDebugMode) {
                    print(e.toString());
                  }
                },
                (data) {
                  if (data) {
                    router.go('/login');
                  }
                },
              );
            },
            icon: const Icon(
              Icons.logout,
              color: Colors.white,
            ),
          ),
        ],
      ),
      bottomSheet: BottomSheet(
        backgroundColor: bgColor,
        onClosing: () {},
        enableDrag: false,
        builder: (context) {
          if (!ref
              .watch(injector<ChangeNotifierProvider<FormsViewModel>>())
              .initialized) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (ref
                  .watch(injector<ChangeNotifierProvider<FormsViewModel>>())
                  .formData ==
              null) {
            return Padding(
              padding: const EdgeInsets.all(defaultPadding * 2),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  const Spacer(),
                  Image.asset(
                    'assets/images/Thoughts-bro 1.png',
                    fit: BoxFit.fill,
                  ),
                  Text(
                    "Nothing is here",
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  Text(
                    ref
                        .watch(
                            injector<ChangeNotifierProvider<FormsViewModel>>())
                        .error,
                    style: Theme.of(context)
                        .textTheme
                        .labelMedium
                        ?.copyWith(color: Colors.red),
                  ),
                  const Spacer(),
                  Row(
                    children: [
                      Expanded(
                        child: FilledButton(
                          onPressed: () {
                            router.push(
                              '/form-data',
                              extra: {
                                'formData': ref
                                    .read(injector<
                                        ChangeNotifierProvider<
                                            FormsViewModel>>())
                                    .formData,
                                'formTemplate': ref
                                    .read(injector<
                                        ChangeNotifierProvider<
                                            FormsViewModel>>())
                                    .formTemplate,
                              },
                            );
                          },
                          child: const Text("Add Data"),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          ref
                                      .read(injector<
                                          ChangeNotifierProvider<
                                              FormsViewModel>>())
                                      .formKey ==
                                  'dsa-form'
                              ? ref
                                  .read(injector<
                                      ChangeNotifierProvider<FormsViewModel>>())
                                  .initialize(formKey: '404')
                              : ref
                                  .read(injector<
                                      ChangeNotifierProvider<FormsViewModel>>())
                                  .initialize(formKey: 'dsa-form');
                        },
                        icon: const Icon(Icons.bug_report),
                      ),
                    ],
                  ),
                ],
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.all(defaultPadding * 2),
              child: Column(
                children: [
                  Expanded(
                    child: FormDataView(
                      formData: ref
                          .read(injector<
                              ChangeNotifierProvider<FormsViewModel>>())
                          .formData!,
                      formTemplate: ref
                          .read(injector<
                              ChangeNotifierProvider<FormsViewModel>>())
                          .formTemplate!,
                    ),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  Text(
                    ref
                        .watch(
                            injector<ChangeNotifierProvider<FormsViewModel>>())
                        .error,
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Colors.red,
                        ),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: FilledButton(
                          onPressed: () {
                            router.push(
                              '/form-data',
                              extra: {
                                'formData': ref
                                    .read(injector<
                                        ChangeNotifierProvider<
                                            FormsViewModel>>())
                                    .formData,
                                'formTemplate': ref
                                    .read(injector<
                                        ChangeNotifierProvider<
                                            FormsViewModel>>())
                                    .formTemplate,
                              },
                            );
                          },
                          child: const Text("Edit"),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          ref
                                      .read(injector<
                                          ChangeNotifierProvider<
                                              FormsViewModel>>())
                                      .formKey ==
                                  'dsa-form'
                              ? ref
                                  .read(injector<
                                      ChangeNotifierProvider<FormsViewModel>>())
                                  .initialize(formKey: '404')
                              : ref
                                  .read(injector<
                                      ChangeNotifierProvider<FormsViewModel>>())
                                  .initialize(formKey: 'dsa-form');
                        },
                        icon: const Icon(Icons.bug_report),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}

class FormDataView extends StatelessWidget {
  final FormDataResponseModel formData;
  final FormTemplateModel formTemplate;
  const FormDataView(
      {super.key, required this.formData, required this.formTemplate});

  @override
  Widget build(BuildContext context) {
    List<FormDataItemModel> agencyItems = formData.payload
        .where((element) => !element.name.startsWith('agencyUser'))
        .toList();
    List<FormDataItemModel> agencyUserItems = formData.payload
        .where((element) => element.name.startsWith('agencyUser'))
        .toList();
    return ListView(
      children: [
        ExpansionTile(
          initiallyExpanded: false,
          collapsedBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          collapsedShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          tilePadding: const EdgeInsets.symmetric(horizontal: defaultPadding),
          childrenPadding: const EdgeInsets.only(
            left: defaultPadding,
            right: defaultPadding,
            bottom: defaultPadding,
          ),
          title: const Text("Agency Details"),
          children: [
            Container(
              decoration: BoxDecoration(
                color: bgColor,
                borderRadius: BorderRadius.circular(8),
              ),
              child: ListView.separated(
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        width: defaultPadding / 2,
                      ),
                      Text(
                        formTemplate.parameters
                            .firstWhere((element) =>
                                element.id == agencyItems[index].id)
                            .displayName,
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium
                            ?.copyWith(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        width: defaultPadding / 2,
                      ),
                      Expanded(
                        child: Text(agencyItems[index].value,
                            textAlign: TextAlign.end),
                      ),
                      const SizedBox(
                        width: defaultPadding / 2,
                      ),
                    ],
                  );
                },
                separatorBuilder: (_, __) => const Divider(),
                itemCount: agencyItems.length,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: defaultPadding,
        ),
        ExpansionTile(
          initiallyExpanded: false,
          collapsedBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          collapsedShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          tilePadding: const EdgeInsets.symmetric(horizontal: defaultPadding),
          childrenPadding: const EdgeInsets.only(
            left: defaultPadding,
            right: defaultPadding,
            bottom: defaultPadding,
          ),
          title: const Text("Agency User Details"),
          children: [
            Container(
              decoration: BoxDecoration(
                color: bgColor,
                borderRadius: BorderRadius.circular(8),
              ),
              child: ListView.separated(
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        width: defaultPadding / 2,
                      ),
                      Text(
                        formTemplate.parameters
                            .firstWhere((element) =>
                                element.id == agencyItems[index].id)
                            .displayName,
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium
                            ?.copyWith(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        width: defaultPadding / 2,
                      ),
                      Expanded(
                        child: Text(agencyUserItems[index].value,
                            textAlign: TextAlign.end),
                      ),
                      const SizedBox(
                        width: defaultPadding / 2,
                      ),
                    ],
                  );
                },
                separatorBuilder: (_, __) => const Divider(),
                itemCount: agencyUserItems.length,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
