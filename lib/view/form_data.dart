import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task/core/constants.dart';
import 'package:task/core/di.dart';
import 'package:task/core/router.dart';
import 'package:task/core/ternary_to_conditional_parser.dart';
import 'package:task/model/client_data.dart';
import 'package:task/model/form_data_item.dart';
import 'package:task/model/form_template.dart';
import 'package:task/repository/form_data.dart';
import 'package:task/repository/login.dart';
import 'package:dart_eval/dart_eval.dart';

class FormDataScreen extends StatefulWidget {
  final FormDataResponseModel? formData;
  final FormTemplateModel? formTemplate;
  const FormDataScreen(
      {super.key, required this.formData, required this.formTemplate});

  @override
  State<FormDataScreen> createState() => _FormDataScreenState();
}

class _FormDataScreenState extends State<FormDataScreen> {
  final formKey = GlobalKey<FormBuilderState>();
  Map<String, List<FormTemplateParameterModel>> sectionedTemplate = {};
  @override
  void initState() {
    super.initState();
    for (var parameter in widget.formTemplate!.parameters) {
      if (sectionedTemplate.containsKey(parameter.category)) {
        sectionedTemplate[parameter.category]!.add(parameter);
      } else {
        sectionedTemplate[parameter.category] = [parameter];
      }
    }
    sectionedTemplate = sectionedTemplate.map((sectionName, sectionItems) {
      var newItems = sectionItems;
      newItems.sort((a, b) => a.displayOrder.compareTo(b.displayOrder));
      return MapEntry(
        sectionName,
        newItems,
      );
    });
  }

  void reEvaluateValueExpressions() {
    widget.formTemplate!.parameters
        .where((parameter) => parameter.valueExpression != null)
        .forEach((parameter) {
      formKey.currentState!.fields[parameter.name]
          ?.didChange(parseValueExpression(
        formKey.currentState!.instantValue,
        parameter.valueExpression!,
        widget.formTemplate!.parameters.map((e) => e.name).toList(),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          'Source Details',
          style: Theme.of(context)
              .textTheme
              .labelLarge
              ?.copyWith(color: Colors.white),
        ),
        backgroundColor: scaffoldBgColor,
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircleAvatar(
            backgroundColor: Colors.white.withOpacity(0.25),
            child: IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () async {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return const AlertDialog(
                    content: CircularProgressIndicator(),
                  );
                },
              );
              final response = await injector<LoginRepository>().logout();
              response.fold(
                (e) {
                  if (kDebugMode) {
                    print(e.toString());
                  }
                },
                (data) {
                  if (data) {
                    router.go('/login');
                  }
                },
              );
            },
            icon: const Icon(
              Icons.logout,
              color: Colors.white,
            ),
          ),
        ],
      ),
      bottomSheet: BottomSheet(
        backgroundColor: bgColor,
        onClosing: () {},
        enableDrag: false,
        builder: (context) {
          if (widget.formTemplate == null) {
            return Padding(
              padding: const EdgeInsets.all(defaultPadding * 2),
              child: Column(
                children: [
                  const Spacer(),
                  Image.asset(
                    'assets/images/Thoughts-bro 1.png',
                    fit: BoxFit.fill,
                  ),
                  Text(
                    "Nothing is here",
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                  const Spacer(),
                ],
              ),
            );
          }
          return Padding(
            padding: const EdgeInsets.all(defaultPadding * 2),
            child: FormBuilder(
              key: formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              initialValue: () {
                Map<String, dynamic> defaultValues = {};
                if (widget.formData != null) {
                  for (var item in widget.formData!.payload) {
                    if (widget.formTemplate!.parameters
                            .firstWhere((element) => element.id == item.id)
                            .dataType ==
                        'Date') {
                      defaultValues[item.name] = DateTime.parse(item.value);
                    } else {
                      defaultValues[item.name] = item.value;
                    }
                  }
                } else {
                  for (var parameter in widget.formTemplate!.parameters) {
                    if (parameter.defaultSelection != null) {
                      defaultValues[parameter.name] =
                          parameter.defaultSelection;
                    } else if (parameter.possibleValues?.isNotEmpty ?? false) {
                      defaultValues[parameter.name] =
                          parameter.possibleValues![0];
                    } else if (parameter.dataType == 'Date') {
                      defaultValues[parameter.name] = DateTime.now();
                    }
                  }
                }
                return defaultValues;
              }(),
              child: Column(
                children: [
                  Expanded(
                    child: ListView.separated(
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              sectionedTemplate.keys.elementAt(index),
                              style: Theme.of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: defaultPadding / 2),
                            ...sectionedTemplate.values
                                .elementAt(index)
                                .map((parameter) {
                              switch (parameter.dataType) {
                                case 'String':
                                  return Visibility(
                                    visible: !parameter.isHidden,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: defaultPadding / 2),
                                      child: FormBuilderTextField(
                                        validator:
                                            FormBuilderValidators.compose([
                                          if (parameter.isMandatory)
                                            FormBuilderValidators.required(
                                                errorText: "Required"),
                                          if (parameter.length > 0)
                                            FormBuilderValidators.max(
                                                parameter.length,
                                                errorText:
                                                    "Max length is ${parameter.length}"),
                                        ]),
                                        readOnly: !parameter.isEditable,
                                        name: parameter.name,
                                        enabled: parameter.isEditable,
                                        decoration: InputDecoration(
                                          labelText: parameter.displayName,
                                        ),
                                      ),
                                    ),
                                  );
                                case "Selection":
                                  return Visibility(
                                    visible: !parameter.isHidden,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: defaultPadding / 2),
                                      child: FormBuilderDropdown(
                                        onChanged: (value) {
                                          setState(() {
                                            reEvaluateValueExpressions();
                                          });
                                        },
                                        name: parameter.name,
                                        enabled: parameter.isEditable,
                                        decoration: InputDecoration(
                                          labelText: parameter.displayName,
                                        ),
                                        validator:
                                            FormBuilderValidators.compose([
                                          if (parameter.isMandatory)
                                            FormBuilderValidators.required(
                                                errorText: "Required"),
                                        ]),
                                        items: parameter.possibleValues
                                                ?.map((value) {
                                              return DropdownMenuItem(
                                                value: value,
                                                child: Text(value),
                                              );
                                            }).toList() ??
                                            [],
                                      ),
                                    ),
                                  );
                                case "Date":
                                  return Visibility(
                                    visible: !parameter.isHidden,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: defaultPadding / 2),
                                      child: FormBuilderDateTimePicker(
                                        onChanged: (value) {
                                          setState(() {
                                            reEvaluateValueExpressions();
                                          });
                                        },
                                        name: parameter.name,
                                        enabled: parameter.isEditable,
                                        inputType: InputType.date,
                                        format: DateFormat("dd-MM-yy"),
                                        decoration: InputDecoration(
                                          labelText: parameter.displayName,
                                        ),
                                        validator:
                                            FormBuilderValidators.compose([
                                          if (parameter.isMandatory)
                                            FormBuilderValidators.required(
                                                errorText: "Required"),
                                        ]),
                                      ),
                                    ),
                                  );
                                case "ConditionalSelection":
                                  return Visibility(
                                    visible: !parameter.isHidden,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: defaultPadding / 2),
                                      child: FormBuilderDropdown(
                                        onChanged: (value) {
                                          setState(() {
                                            reEvaluateValueExpressions();
                                          });
                                        },
                                        name: parameter.name,
                                        enabled: parameter.isEditable,
                                        decoration: InputDecoration(
                                          labelText: parameter.displayName,
                                        ),
                                        validator:
                                            FormBuilderValidators.compose([
                                          if (parameter.isMandatory)
                                            FormBuilderValidators.required(
                                                errorText: "Required"),
                                        ]),
                                        items: parameter
                                                .possibleValuesMap!
                                                .value[formKey.currentState!
                                                    .getRawValue(parameter
                                                        .possibleValuesMap!
                                                        .dependantFieldName)]
                                                ?.map((value) {
                                              return DropdownMenuItem(
                                                value: value,
                                                child: Text(value),
                                              );
                                            }).toList() ??
                                            [],
                                      ),
                                    ),
                                  );
                                default:
                                  return const SizedBox();
                              }
                            }),
                          ],
                        );
                      },
                      separatorBuilder: (_, __) => const SizedBox(
                        height: defaultPadding,
                      ),
                      itemCount: sectionedTemplate.keys.length,
                    ),
                  ),
                  const SizedBox(
                    height: defaultPadding,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: FilledButton(
                          onPressed: () async {
                            if (formKey.currentState!.saveAndValidate()) {
                              showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return const Center(
                                      child: CircularProgressIndicator());
                                },
                              );
                              final response = widget.formData == null
                                  ? await injector<FormDataRepository>()
                                      .postFormData(
                                      injector<SharedPreferences>()
                                              .getString('accessToken') ??
                                          "",
                                      "dsa-form",
                                      ClientDataModel.fromJson(jsonDecode(
                                              injector<SharedPreferences>()
                                                      .getString(
                                                          'clientData') ??
                                                  ""))
                                          .clientId,
                                      FormDataRequestModel(
                                        payload: formKey
                                            .currentState!.fields.entries
                                            .map((entry) {
                                          return FormDataItemModel(
                                            id: widget.formTemplate!.parameters
                                                .firstWhere((element) =>
                                                    element.name == entry.key)
                                                .id,
                                            name: entry.key,
                                            value: entry.value.value.toString(),
                                          );
                                        }).toList(),
                                        leadId: 12345,
                                      ),
                                    )
                                  : await injector<FormDataRepository>()
                                      .updateFormData(
                                      injector<SharedPreferences>()
                                              .getString('accessToken') ??
                                          "",
                                      "dsa-form",
                                      ClientDataModel.fromJson(jsonDecode(
                                              injector<SharedPreferences>()
                                                      .getString(
                                                          'clientData') ??
                                                  ""))
                                          .clientId,
                                      FormDataRequestModel(
                                        payload: formKey
                                            .currentState!.fields.entries
                                            .map((entry) {
                                          return FormDataItemModel(
                                            id: widget.formTemplate!.parameters
                                                .firstWhere((element) =>
                                                    element.name == entry.key)
                                                .id,
                                            name: entry.key,
                                            value: entry.value.value.toString(),
                                          );
                                        }).toList(),
                                        leadId: 12345,
                                      ),
                                    );
                              response.fold(
                                (e) {
                                  if (kDebugMode) {
                                    print(e.toString());
                                  }
                                },
                                (data) {
                                  Navigator.of(context).pop();
                                  Navigator.of(context).pop();
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Saved successfully"),
                                    ),
                                  );
                                },
                              );
                            }
                          },
                          child: const Text("Save"),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

String parseValueExpression(Map<String, dynamic> state, String valueExpression,
    List<String> parameters) {
  String parsed = valueExpression
      .replaceAll('\n', '')
      .replaceAll('\\', '')
      .replaceAll("null", "\"\"");
  for (var element in parameters) {
    parsed = parsed.replaceAll("#$element",
        "\"${(state.containsKey(element) ? state[element].toString() : "")}\"");
  }
  parsed = ternaryToConditionalParser(parsed).replaceAll('};', '}');
  parsed = "String? test(){$parsed}";
  return eval(
    parsed,
    function: 'test',
  ).toString();
}
