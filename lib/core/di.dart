import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task/repository/access_token.dart';
import 'package:task/repository/client_data.dart';
import 'package:task/repository/form_data.dart';
import 'package:task/repository/form_template.dart';
import 'package:task/repository/login.dart';
import 'package:task/view_model/forms.dart';
import 'package:task/view_model/login.dart';

final injector = GetIt.instance;

Future<void> setupDi() async {
  /// Shared Preferences
  final prefs = await SharedPreferences.getInstance();
  injector.registerSingleton(prefs);

  /// Repositories
  final loginRepository = LoginRepository();
  injector.registerSingleton<LoginRepository>(loginRepository);
  final accessTokenRepository = AccessTokenRepository();
  injector.registerSingleton<AccessTokenRepository>(accessTokenRepository);
  final clientDataRepository = ClientDataRepository();
  injector.registerSingleton<ClientDataRepository>(clientDataRepository);
  final formDataRepository = FormDataRepository();
  injector.registerSingleton<FormDataRepository>(formDataRepository);
  final formTemplateRepository = FormTemplateRepository();
  injector.registerSingleton<FormTemplateRepository>(formTemplateRepository);

  /// Pods
  final loginPod = ChangeNotifierProvider<LoginViewModel>(
    (ref) {
      return LoginViewModel(
        loginRepository: injector<LoginRepository>(),
        accessTokenRepository: injector<AccessTokenRepository>(),
      );
    },
  );
  injector.registerSingleton<ChangeNotifierProvider<LoginViewModel>>(loginPod);
  final formsPod = ChangeNotifierProvider<FormsViewModel>(
    (ref) {
      return FormsViewModel(
        formDataRepository: injector<FormDataRepository>(),
        formTemplateRepository: injector<FormTemplateRepository>(),
      );
    },
  );
  injector.registerSingleton<ChangeNotifierProvider<FormsViewModel>>(formsPod);
}
