import 'package:go_router/go_router.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task/core/di.dart';
import 'package:task/model/form_data_item.dart';
import 'package:task/model/form_template.dart';
import 'package:task/view/form.dart';
import 'package:task/view/form_data.dart';
import 'package:task/view/login.dart';

final router = GoRouter(
  routes: [
    GoRoute(
      redirect: (context, state) {
        if (injector<SharedPreferences>().getString('accessToken') !=
            'd3893185-d591-4087-a170-bdcda05692a4') {
          return '/login';
        } else {
          return null;
        }
      },
      path: '/',
      builder: (context, state) => const FormScreen(),
    ),
    GoRoute(
        path: '/form-data',
        redirect: (context, state) {
          if (!((state.extra as Map<String, dynamic>).containsKey('formData') &&
              (state.extra as Map<String, dynamic>)
                  .containsKey('formTemplate'))) {
            return '/';
          }
          return null;
        },
        builder: (context, state) {
          if ((state.extra as Map<String, dynamic>).containsKey('formData') &&
              (state.extra as Map<String, dynamic>)
                  .containsKey('formTemplate')) {
            return FormDataScreen(
              formData: (state.extra as Map<String, dynamic>)['formData']
                  as FormDataResponseModel?,
              formTemplate:
                  (state.extra as Map<String, dynamic>)['formTemplate']
                      as FormTemplateModel?,
            );
          } else {
            return const FormScreen();
          }
        }),
    GoRoute(
      path: '/login',
      builder: (context, state) {
        return const LoginScreen();
      },
    ),
  ],
);
