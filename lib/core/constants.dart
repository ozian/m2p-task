import 'package:flutter/material.dart';

const Color scaffoldBgColor = Color(0xFF0D47A1);
const Color bgColor = Color(0xFFF5FAFF);
const Color buttonBgColor = Color(0xFF2196F3);
const Color borderColor = Color(0xFFD5EAFB);
const Color hintColor = Color(0xFFC8D4DD);
const double defaultPadding = 16.0;
