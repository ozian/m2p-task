List firstSplit(String input, String delimiter) {
  var parts = input.split(delimiter);
  return [parts[0], parts.sublist(1, parts.length).join(delimiter)];
}

String ternaryToConditionalParser(String ternary) {
  var parts = firstSplit(ternary, "?");
  var condition = parts[0];
  var trueValue = firstSplit(parts[1], ':')[0];
  var falseValue = ternary.split('?').length > 2
      ? ternaryToConditionalParser(firstSplit(parts[1], ':')[1])
      : parts[1].split(":")[1];
  if (ternary.split('?').length > 2) {
    return "if ($condition) { return $trueValue; } else { $falseValue; }";
  }
  return "if ($condition) { return $trueValue; } else { return $falseValue; }";
}
