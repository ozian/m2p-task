import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task/core/di.dart';
import 'package:task/core/router.dart';
import 'package:task/model/client_data.dart';
import 'package:task/model/form_data_item.dart';
import 'package:task/model/form_template.dart';
import 'package:task/repository/form_data.dart';
import 'package:task/repository/form_template.dart';

class FormsViewModel with ChangeNotifier {
  final FormDataRepository formDataRepository;
  final FormTemplateRepository formTemplateRepository;
  FormsViewModel({
    required this.formDataRepository,
    required this.formTemplateRepository,
  });
  bool initialized = false;
  String formKey = '';
  String error = '';
  ClientDataModel? clientData;
  FormDataResponseModel? formData;
  FormTemplateModel? formTemplate;

  void updateError(String value) {
    error = value;
    notifyListeners();
  }

  void initialize({String formKey = 'dsa-form'}) async {
    this.formKey = formKey;
    if (initialized) {
      initialized = false;
      notifyListeners();
    }

    /// fetching client data
    final clientData = injector<SharedPreferences>().getString('clientData');
    final accessToken = injector<SharedPreferences>().getString('accessToken');
    if (clientData == null || accessToken == null) {
      await injector<SharedPreferences>().remove('clientData');
      await injector<SharedPreferences>().remove('accessToken');
      this.formKey = '';
      router.go('/login');
      return;
    } else {
      this.clientData = ClientDataModel.fromJson(jsonDecode(clientData));
    }

    /// fetching form data
    final remoteFormData = await formDataRepository.getFormData(
        accessToken, formKey, this.clientData!.clientId);
    remoteFormData.fold((error) {
      if (error.message == '404') {
        formData = null;
      } else {
        this.error = error.message;
        formData = null;
      }
    }, (data) {
      formData = data;
      error = "";
      formTemplate = null;
    });

    /// fetching form template
    final remoteFormTemplate = await formTemplateRepository.getFormTemplate(
        accessToken, 'dsa-form', this.clientData!.clientId);
    remoteFormTemplate.fold((error) {
      this.error = error.message;
      initialized = true;
    }, (data) {
      formTemplate = data;
      error = "";
      initialized = true;
    });
    notifyListeners();
  }

  void clear() {
    formKey = '';
    error = '';
    clientData = null;
    formData = null;
    formTemplate = null;
    initialized = false;
    notifyListeners();
  }
}
