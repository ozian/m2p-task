import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task/core/di.dart';
import 'package:task/core/router.dart';
import 'package:task/repository/access_token.dart';
import 'package:task/repository/client_data.dart';
import 'package:task/repository/login.dart';

class LoginViewModel with ChangeNotifier {
  final LoginRepository loginRepository;
  final AccessTokenRepository accessTokenRepository;
  LoginViewModel({
    required this.loginRepository,
    required this.accessTokenRepository,
  });
  String mobileNumber = '';
  bool isLoading = false;
  String error = '';
  String otp = '';
  bool isOtpScreen = false;

  void updateLoading(bool value) {
    isLoading = value;
    notifyListeners();
  }

  void updateError(String value) {
    error = value;
    notifyListeners();
  }

  void login(String mobileNumber) async {
    updateLoading(true);
    this.mobileNumber = mobileNumber;
    final result = await loginRepository.login(mobileNumber);
    result.fold((error) {
      updateError(error.message);
      updateLoading(false);
    }, (data) {
      otp = '';
      isOtpScreen = true;
      updateError("");
      updateLoading(false);
    });
    notifyListeners();
  }

  void returnToLogin() {
    isOtpScreen = false;
    notifyListeners();
  }

  void getAccessToken(String otp) async {
    updateLoading(true);
    this.otp = otp;
    final result =
        await accessTokenRepository.getAccessToken(mobileNumber, otp);
    result.fold((error) {
      updateError(error.message);
      updateLoading(false);
    }, (data) async {
      updateError("");
      mobileNumber = '';
      otp = '';
      isOtpScreen = false;
      await injector<SharedPreferences>()
          .setString('accessToken', data.access_token);

      /// fetching client data
      final clientData = await injector<ClientDataRepository>()
          .getClientData(data.access_token);
      clientData.fold((error) async {
        updateError(error.message);
        await injector<SharedPreferences>().remove('accessToken');
        updateLoading(false);
      }, (data) async {
        await injector<SharedPreferences>()
            .setString('clientData', jsonEncode(data.toJson()));
        updateLoading(false);
        router.go('/');
      });
    });
    notifyListeners();
  }
}
